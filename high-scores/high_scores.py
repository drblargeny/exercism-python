from typing import List


def latest(scores: List):
    """Return the latest score (i.e. the value at the last index)

    scores - list of scores, can be None
    """
    return None if scores is None or len(scores) == 0 else scores[-1]


def personal_best(scores: List):
    """Return the best/highest score

    scores - list of scores, can be None
    """
    sorted_scores = _sort_scores(scores)
    return sorted_scores[0] if len(sorted_scores) > 0 else None


def _sort_scores(scores: List) -> List:
    """Return a sorted copy of the scores with the highest score at the first
    index.

    scores - list of scores, can be None. Will not be altered.
    """
    if scores is None or len(scores) == 0:
        return []
    else:
        sorted_scores = scores.copy()
        sorted_scores.sort(reverse=True)
        return sorted_scores


def personal_top_three(scores: List) -> List:
    """Return a sorted copy of the highest three scores with the highest score
    at the first index.

    scores - list of scores, can be None. Will not be altered.
    """
    return _personal_top(3, scores)


def _personal_top(count: int, scores: List) -> List:
    """Return a sorted copy of the {count} highest scores with the highest score
    at the first index.

    count - the number of scores to return
    scores - list of scores, can be None. Will not be altered.
    """
    sorted_scores = _sort_scores(scores)
    if len(sorted_scores) >= count:
        return sorted_scores[0:count]
    else:
        return sorted_scores
