def two_fer(name='you'):
    """Return 'One for X, one for me.'

    The X is replaced by the value of the name parameter.
    If the name parameter is empty, 'you' will be used
    """
    if len(name) == 0 or name.isspace():
        name = 'you'

    return f'One for {name}, one for me.'
