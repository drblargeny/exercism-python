def convert(number):
    """Convert a number into a string that contains raindrop sounds corresponding to certain potential factors.

    The rules of `raindrops` are that if a given number:

    - has 3 as a factor, add 'Pling' to the result.
    - has 5 as a factor, add 'Plang' to the result.
    - has 7 as a factor, add 'Plong' to the result.
    - _does not_ have any of 3, 5, or 7 as a factor, the result should be the digits of the number.
    """
    
    if number % 3 == 0:
        result = 'Pling'
    else:
        result = ''
    
    if number % 5 == 0:
        result += 'Plang'
    
    if number % 7 == 0:
        result += 'Plong'
    
    if result == '':
        return str(number)
    else:
        return result
